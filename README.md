Ensure that you have SSH access to the Colibri module.

Clone the repository and its submodules using:

     git clone git@gitlab.com:inf0rm4tik3r-Colibri/Dev-Parent.git --recurse-submodules
